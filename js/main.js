function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

jQuery.noConflict();

jQuery(document).ready(function() {

	jQuery('.faq-title').bind('click', function(){
		let text = jQuery(this).siblings('.faq-text').first();
		let parent = jQuery(this).parent();
		if(parent.hasClass('expanded')){
			parent.removeClass('expanded');
			text.slideUp();
		} else {
			parent.addClass('expanded');
			text.slideDown();
		}
	});

	jQuery('.footer-call-us.desktop-only').bind('click', function(){
		jQuery(this).find('.call-us-text p').toggle();
	});

	// jQuery('input[type="tel"]').bind('keydown', function(e){
	// 	if( 
	// 		(e.key && e.key !== "Backspace" && e.key !== "Meta" && isNaN(parseInt(e.key)))
	// 		) {
	// 		e.preventDefault();
	// 	}
	// });

	jQuery('input[type="tel"]').bind('keyup', function(){
		var value = jQuery(this).val().replace(/[^\d\.]/g, '');
		jQuery(this).val(value);
	});

	jQuery(".validate-form").bind('keyup', function(e){
		let inputs = jQuery(this).find('input');
		let valid = true;
		for(let input of inputs){
			if(input.value === "") {
				valid = false;
				break;
			}
		}

		let submit_button = jQuery(this).find('input[type="submit"]')
		if(valid){
			submit_button.removeAttr('disabled');
		} else {
			submit_button.attr('disabled', '');
		}

	});

	jQuery(".show-password").click(function() {
		jQuery(this).toggleClass("pwd");
		var input = jQuery(".pass");
		jQuery(this).find('span').toggle();
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

	jQuery("#validate").bind("click", function(e) {
		var email = jQuery(".username").val();
		if (validateEmail(email)) {
			jQuery(".error").css("display", "none");
			jQuery(".username").css("color", "#252c32");
		} else {
			jQuery(".error").css("display", "block");
			jQuery(".username").css("color", "#ff2610");
		}
		return false;
	});
});